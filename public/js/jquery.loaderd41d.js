jQuery(window).bind('load', function() {
	jQuery('.foreground').toggle('slow');
});

jQuery(function() {
	jQuery('.view-portfolio .views-field-field-portfolio-image a').hide();
});
jQuery(window).bind('load', function() {
	 var i = 1;
	 var imgs = jQuery('.view-portfolio .views-field-field-portfolio-image a').length;
	 var int = setInterval(function() {
		 //console.log(i); check to make sure interval properly stops
		 if(i >= imgs) clearInterval(int);
		 jQuery('.view-portfolio .views-field-field-portfolio-image a:hidden').eq(0).fadeIn(300);
		 i++;
	 }, 300);
});


jQuery(function(){
	jQuery('.view-portfolio .views-row .views-field-field-portfolio-image').prepend('<span class="magnify"></span>');
	jQuery('.view-portfolio .views-row .views-field-field-portfolio-image').hover(function(){
		jQuery(this).find('img').stop().animate({opacity:.5}).parents('.views-row').find('.magnify').stop().animate({"right":"0", "bottom":"1"},100)
		},
		function(){
			jQuery(this).find('img').stop().animate({opacity:1}).parents('.views-row').find('.magnify').stop().animate({"right":"-45", "bottom":"1"}, 100)
			}
			)
	})